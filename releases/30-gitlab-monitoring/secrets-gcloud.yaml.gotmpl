---
{{ $env_prefix := .Environment.Values | getOrNil "env_prefix" | default .Environment.Name }}

{{- $gcloud_brand := exec "gcloud" (list "--project" .Environment.Values.google_project "--format=value(name)" "alpha" "iap" "oauth-brands" "list") }}
{{- $oauth_name := .Values | getOrNil "gitlab_monitoring.secrets.oauth_name" | default "monitoring" }}
{{- $gcloud_monitoring_oauth_client_id := (split "/" (exec "gcloud" (list "--verbosity" "error" "--project" .Environment.Values.google_project "--format=value(name)" "alpha" "iap" "oauth-clients" "list" $gcloud_brand (printf "--filter=displayName=%s" $oauth_name ))))._5 }}
{{- $gcloud_monitoring_oauth_client_secret := exec "gcloud" (list "--verbosity" "error" "--project" .Environment.Values.google_project "--format=value(secret)" "alpha" "iap" "oauth-clients" "list" $gcloud_brand (printf "--filter=displayName=%s" $oauth_name )) }}

prometheus_oauth:
  client_id: {{ $gcloud_monitoring_oauth_client_id }}
  client_secret: {{ $gcloud_monitoring_oauth_client_secret }}

alertmanager_oauth:
  client_id: {{ $gcloud_monitoring_oauth_client_id }}
  client_secret: {{ $gcloud_monitoring_oauth_client_secret }}

thanos_objectstore_yaml: {{ exec "bash" (list "-c" (print "gsutil cat gs://gitlab-configs/" $env_prefix "-thanos-secret.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-")) | quote }}

{{- if not (.Values | getOrNil "gitlab_monitoring.google_managed_cert.enabled" | default false) }}
{{- $frontend_loadbalancer_secrets := exec "bash" (list "-c" (print "gsutil cat gs://gitlab-" $env_prefix "-secrets/frontend-loadbalancer/" $env_prefix ".enc | gcloud --project " .Environment.Values.google_project " kms decrypt --location global --keyring=gitlab-secrets --key " $env_prefix " --ciphertext-file=- --plaintext-file=-")) }}

prometheus_gke_tls:
  crt: {{ $frontend_loadbalancer_secrets | exec "jq" (list ".\"gitlab-haproxy\".ssl.internal_crt") }}
  key: {{ $frontend_loadbalancer_secrets | exec "jq" (list ".\"gitlab-haproxy\".ssl.internal_key") }}

alertmanager_gke_tls:
  crt: {{ $frontend_loadbalancer_secrets | exec "jq" (list ".\"gitlab-haproxy\".ssl.internal_crt") }}
  key: {{ $frontend_loadbalancer_secrets | exec "jq" (list ".\"gitlab-haproxy\".ssl.internal_key") }}
{{- end }}
