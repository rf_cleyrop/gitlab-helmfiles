---

{{- $prom_req_cpu := .Environment.Values | getOrNil "prometheus_requests_cpu" | default "6" }}
{{- $prom_req_memory := .Environment.Values | getOrNil "prometheus_requests_memory" | default "180Gi" }}
{{- $prom_limits_memory := .Environment.Values | getOrNil "prometheus_limits_memory" | default "200Gi" }}

prometheus:
  prometheusSpec:
    enableFeatures:
      # https://prometheus.io/docs/prometheus/latest/feature_flags/#memory-snapshot-on-shutdown
      - memory-snapshot-on-shutdown
    resources:
      requests:
        cpu: "{{ $prom_req_cpu }}"
        memory: {{ $prom_req_memory }}
      limits:
        memory: {{ $prom_limits_memory }}
    nodeSelector:
      type: {{ .Environment.Values | getOrNil "prometheus_node_type" | default "default" }}
    storageSpec:
      volumeClaimTemplate:
        spec:
          storageClassName: ssd
          resources:
            requests:
              storage: 250Gi
  ingress:
    hosts:
      - "prometheus-gke.{{ .Environment.Name }}.gitlab.net"
    paths:
      - /*
  service:
    loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=prometheus-gke-%s" .Environment.Name) "--format" "value(address)") }}
