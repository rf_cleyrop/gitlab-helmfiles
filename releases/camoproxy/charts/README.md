# CamoProxy Chart

This (still) is a simple chart to deploy CamoProxy

To spin this up in your K8s cluster:

1. Create a release in helm by running:
   
    ```
    helm upgrade --install camoproxy1 ../camoproxy -n cpt1 --debug
    ```

    `cpt1` is the name of my namespace.

1. Sign a URL with the [url-tool](https://github.com/cactus/go-camo#url-tool) to get the suffix of the CamoProxy URL for http://golang.org/doc/gopher/frontpage.png and using the HMAC key `test` (the key needs to match the key that is passed to CamoProxy as the first argument - see `values.yaml`). 

    ``` 
    k exec -ti camoproxy1-599df877ff-qdthg -n cpt1 -- /bin/url-tool -k "test" encode -b base64 "http://golang.org/doc/gopher/frontpage.png"
    ```

    The result will be something like: `/D23vHLFHsOhPOcvdxeoQyAJTpvM/aHR0cDovL2dvbGFuZy5vcmcvZG9jL2dvcGhlci9mcm9udHBhZ2UucG5n`.  
    
    `camoproxy1-599df877ff-qdthg` is the name of the CamoProxy pod.

1. Forward your 8080 port to port 8080 on your K8s cluster so you can directly hit CamoProxy from your browser:
   
    ```
    kubectl port-forward  -n cpt1 camoproxy1-599df877ff-qdthg 8080:8080
    ```

    When using a `LoadBalancer` service type, you can also grab the extenal endpoint to have acess to CamoProxy (in the GCP console, in the Project where your K8s cluster resides: `Kubernetes Engine` -> `Services & Ingress` -> Click `Camoproxy` from the Services list -> `External Endpoints`)

2. Put `0.0.0.0:8080/D23vHLFHsOhPOcvdxeoQyAJTpvM/aHR0cDovL2dvbGFuZy5vcmcvZG9jL2dvcGhlci9mcm9udHBhZ2UucG5n` in your browser and CamoProxy will proxy your request to http://golang.org/doc/gopher/frontpage.png (image of a gopher).

That's it.

Note: If you enable `prometheus_monoitoring` you'll also need to have installed the Prometheus Operator to be able to create CRDs (`kubectl create -f https://raw.githubusercontent.com/prometheus-operator/prometheus-operator/master/bundle.yaml`)