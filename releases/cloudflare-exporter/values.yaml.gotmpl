---
nameOverride: "cloudflare-exporter"

deployment:
  image:
    repository: "registry.gitlab.com/gitlab-org/cloudflare_exporter"
    tag: "{{ .Values | getOrNil "cloudflare_exporter.tag" | default "v0.3.0" }}"
  env:
    CLOUDFLARE_SCRAPE_INTERVAL_SECONDS: "{{ .Values | getOrNil "cloudflare_exporter.interval_seconds" | default "300" }}"
    CLOUDFLARE_EXPORTER_SCRAPE_TIMEOUT_SECONDS: "180"
    CLOUDFLARE_EXPORTER_LOG_LEVEL: "debug"
    CLOUDFLARE_ZONES: "{{ .Values.cloudflare_exporter.zones }}"
    CLOUDFLARE_EXPORTER_INITIAL_SCRAPE_IMMEDIATELY: "{{ .Values | getOrNil "cloudflare_exporter.scrape_immediately" | default "false" }}"
  ports:
    - name: "metrics"
      containerPort: 9199
  probes:
    readiness:
      httpGet:
        port: "metrics"
        path: "/metrics"
  resources:
    requests:
      cpu: 250m
      memory: 256Mi
    limits:
      memory: 1Gi

service:
  enabled: true
  headless: true
  ports:
    - name: "metrics"
      targetPort: "metrics"
      port: 9199

prometheus_monitoring:
  enabled: true
  scrape_interval: "30s"
  relabelings:
    - targetLabel: type
      replacement: waf
    - targetLabel: tier
      replacement: lb
    - targetLabel: stage
      replacement: main
    - sourceLabels:
        - __meta_kubernetes_pod_node_name
      targetLabel: node

secrets:
  values:
{{ if .Values | getOrNil "local_secrets" | default false -}}
{{ readFile "private/secrets.yaml" | indent 4 -}}
{{- else -}}
{{ exec "bash" (list "-c" "gsutil cat gs://gitlab-configs/k8s_cloudflare_exporter_secret.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-") | indent 4 -}}
{{- end -}}
