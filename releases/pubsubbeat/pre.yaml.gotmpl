---
instances:
- topic: pubsub-camoproxy-inf-pre
- topic: pubsub-consul-inf-pre
- topic: pubsub-fluentd-inf-pre
- topic: pubsub-gitaly-inf-pre
- topic: pubsub-gke-audit-inf-pre
- topic: pubsub-gke-inf-pre
- topic: pubsub-gke-systemd-inf-pre
- topic: pubsub-jaeger-inf-pre
- topic: pubsub-kas-inf-pre
- topic: pubsub-mailroom-inf-pre
- topic: pubsub-monitoring-inf-pre
- topic: pubsub-pages-inf-pre
- topic: pubsub-pubsubbeat-inf-pre
- topic: pubsub-rails-inf-pre
- topic: pubsub-registry-inf-pre
- topic: pubsub-runner-inf-pre
- topic: pubsub-shell-inf-pre
- topic: pubsub-sidekiq-inf-pre
- topic: pubsub-system-inf-pre
- topic: pubsub-vault-inf-pre
- topic: pubsub-workhorse-inf-pre

deployment:
  resources:
    limits:
      cpu: 200m
      memory: 250M
    requests:
      cpu: 200m
      memory: 250M
  env:
    GOMAXPROCS: "1"
  resources_exporter:
    limits:
      cpu: 250m
      memory: 100M
    requests:
      cpu: 250m
      memory: 100M
  resources_mtail:
    limits:
      cpu: 250m
      memory: 100M
    requests:
      cpu: 250m
      memory: 100M

serviceAccount:
  annotations:
    iam.gke.io/gcp-service-account: "pubsubbeat-k8s@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com"

pubsubbeat:
  es:
    installed: {{ .Values | getOrNil "pubsubbeat.es.installed" | default false }}
  json:
    use_number: true

secrets:
  values:
{{ if .Values | getOrNil "local_secrets" | default false -}}
{{ readFile "private/secrets.yaml" | indent 4 -}}
{{- else -}}
{{ exec "bash" (list "-c" "gsutil cat gs://gitlab-configs/pre_pubsubbeat.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-") | indent 4 -}}
{{- end -}}
