---
instances:
- topic: pubsub-camoproxy-inf-gprd
- topic: pubsub-consul-inf-gprd
- topic: pubsub-fluentd-inf-gprd
- topic: pubsub-gitaly-inf-gprd
- topic: pubsub-gcp-events-inf-gprd
- topic: pubsub-gke-inf-gprd
- topic: pubsub-gke-audit-inf-gprd
- topic: pubsub-gke-systemd-inf-gprd
- topic: pubsub-kas-inf-gprd
- topic: pubsub-mailroom-inf-gprd
- topic: pubsub-monitoring-inf-gprd
- topic: pubsub-pages-inf-gprd
- topic: pubsub-postgres-inf-gprd
- topic: pubsub-postgres-inf-db-benchmarking
  google_project: gitlab-db-benchmarking
- topic: pubsub-praefect-inf-gprd
- topic: pubsub-pubsubbeat-inf-gprd
- topic: pubsub-puma-inf-gprd
- topic: pubsub-pvs-inf-gprd
- topic: pubsub-rails-inf-gprd
- topic: pubsub-redis-inf-gprd
- topic: pubsub-registry-inf-gprd
- topic: pubsub-runner-inf-gprd
- topic: pubsub-shell-inf-gprd
- topic: pubsub-sidekiq-inf-gprd
- topic: pubsub-system-inf-gprd
- topic: pubsub-system-inf-db-benchmarking
  google_project: gitlab-db-benchmarking
- topic: pubsub-workhorse-inf-gprd

deployment:
  resources:
    # these are based on peak usage on pubsubbeat VMs in production
    limits:
      cpu: 1500m
      memory: 1G  # beats keep messages in queues in memory, so RAM serves as a buffer before sending bulk requests to the ES cluster, that's why they can grow quite a lot
    requests:
      cpu: 1000m
      memory: 1G
  env:
    GOMAXPROCS: "2"
  resources_exporter:
    limits:
      cpu: 500m
      memory: 200M
    requests:
      cpu: 500m
      memory: 200M
  resources_mtail:
    limits:
      cpu: 500m
      memory: 200M
    requests:
      cpu: 500m
      memory: 200M

serviceAccount:
  annotations:
    iam.gke.io/gcp-service-account: "pubsubbeat-k8s@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com"

pubsubbeat:
  es:
    installed: {{ .Values | getOrNil "pubsubbeat.es.installed" | default false }}
  json:
    use_number: true

secrets:
  values:
{{ if .Values | getOrNil "local_secrets" | default false -}}
{{ readFile "private/secrets.yaml" | indent 4 -}}
{{- else -}}
{{ exec "bash" (list "-c" "gsutil cat gs://gitlab-configs/gprd_pubsubbeat.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-") | indent 4 -}}
{{- end -}}
