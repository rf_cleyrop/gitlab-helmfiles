# consul release

## Customizations

We've modified through the use of `kustomize` the configuration of the
deployment and added a `hostPort` to the Daemonset.  This forces all Pods to
listen to port `8600` on every node allowing us to query consul running on the
instance itself.

The downside to this, is that for all updates to the consul deployment, consul
will be unavailable for the time it takes to complete cycling the replacement
Pod on a given node.

## External Requirements

### Minikube

There are no external requirements for minikube

### GitLab.com environments

* For fetching secrets gkms and jq are required with access permissions to the gkms vault
* The gossip key is fetched from Chef, this should eventually be moved to gkms https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10906

## Development

For the configuration in minikube:

* TLS is disabled so there are no secrets or the gossip key
* The server is also enabled where when running in prod and non-prod environments the consul server is running on VMs

Run with the minikube Helmfile environment, e.g. `helmfile -e minikube apply`
